import os, sys
import argparse
import ROOT
from LHCbStyle import setLHCbStyle
setLHCbStyle()

def parse_args():
    parser = argparse.ArgumentParser(description='Script for plotting track pulls.')
    parser.add_argument('--input', type=str, default=None, required=True,
                        help='Input ROOT file.')
    parser.add_argument('--output', type=str, default=None, required=True,
                        help='Output ROOT file.')
    return parser.parse_args()


def plot_track_pulls(fname, oname):
    '''
    Plot pulls of track variables.
    '''
    f = ROOT.TFile(fname)
    of = ROOT.TFile(oname, 'recreate')
    
    pt = ROOT.TPaveText(0.15, 0.75, 0.41, 0.9, 'BRNDC')
    pt.SetTextSize(0.055)
    pt.SetBorderSize(1)
    pt.SetFillColor(0)
    pt.SetFillStyle(0)
    pt.SetLineWidth(0)

    pull_vars = ['xpull', 'ypull', 'txpull', 'typull']
    for pull_var in pull_vars:
        hname = 'Track/TrackResChecker/ALL/vertex/' + pull_var
        h = f.Get(hname)
        hnew = ROOT.TH1F('h_'+pull_var+'_new', '', h.GetNbinsX(), h.GetBinLowEdge(1),
                         h.GetBinLowEdge(h.GetNbinsX()) + h.GetBinWidth(h.GetNbinsX()))
        for i in range(1, h.GetNbinsX() + 1):
            hnew.SetBinContent(i, h.GetBinContent(i))
            hnew.SetBinError(i, h.GetBinError(i))
        hnew.Draw('E1 p1')
        hnew.SetXTitle(pull_var)
        c = ROOT.TCanvas('c_'+pull_var, 'c_'+pull_var)
        func = ROOT.TF1('f_'+pull_var, 'gaus(0)', -5, 5)
        func.SetLineColor(ROOT.kRed)
        hnew.Fit('f_' + pull_var, 'R')
        pt.Clear()
        pt.AddText('#mu: ' + '{:.3f}#pm{:.3f}'.format(func.GetParameter(1), func.GetParError(1)))
        pt.AddText('#sigma: ' + '{:.3f}#pm{:.3f}'.format(func.GetParameter(2), func.GetParError(2)))
        pt.Draw()
        c.Write()

    of.Write()
    of.Close()
    f.Close()

if __name__ == '__main__':
    args = parse_args()
    plot_track_pulls(args.input, args.output)
