import os, sys
import argparse
import ROOT
from LHCbStyle import setLHCbStyle
setLHCbStyle()

def parse_args():
    parser = argparse.ArgumentParser(description='Script for plotting IP resolution.')
    parser.add_argument('--input', type=str, default=None, required=True,
                        help='Input ROOT file.')
    parser.add_argument('--output', type=str, default=None, required=True,
                        help='Output ROOT file.')
    parser.add_argument('--sel', type=str, default=None,
                        help='Track selection.')
    return parser.parse_args()
    
def plot_ip_res(fname, oname, sel=None):
    '''
    Plot IP resolutions using both the true PV and the reconstructed
    PV and covariance matrix.
    '''

    res_vars = ['IPx','IPy','recIPx','recIPy']
    
    f = ROOT.TFile(fname, 'read')
    of = ROOT.TFile(oname, 'recreate')
    t = f.Get('TrackIPResolutionCheckerNT/tracks')
    of.cd()
    
    # By default select long tracks matched to prompt particles.
    if sel==None:
        sel = 'trueeta>2 && trueeta<5 && typeofprefix==0 && type==3'

    pt = ROOT.TPaveText(0.22, 0.65, 0.48, 0.9, 'BRNDC')
    pt.SetTextSize(0.055)
    pt.SetBorderSize(1)
    pt.SetFillColor(10)
    pt.SetLineWidth(0)
        
    for res_var in res_vars:
        h = ROOT.TH2F('h_'+res_var, 'h_'+res_var, 20, 0, 3, 100, -300, 300)
        draw_var = res_var + '*1000.:1./truept'
        t.Draw(draw_var+'>>h_'+res_var, sel, 'goff')
        h.FitSlicesY()
        res = of.Get(h.GetName()+'_2')
        res.SetTitle('')
        c = ROOT.TCanvas('c_'+res_var,'c_'+res_var)
        res.Draw('E1 p1')
        res.GetYaxis().SetTitle('#sigma('+res_var+') [#mum]')
        res.GetXaxis().SetTitle('1/#it{p}_{T} [c/GeV]')
        res.GetYaxis().SetRangeUser(0, 60)
        func = ROOT.TF1('f_'+res_var, 'pol1', 0, 3)
        res.Fit('f_'+res_var, 'R')
        p0 = format(func.GetParameter(0), '.2f')
        p1 = format(func.GetParameter(1), '.2f')
        txt = '#sigma(' + res_var +')=' + p0 + '+' + p1 + '/#it{p}_{T}'
        pt.Clear()
        pt.AddText(txt)
        pt.Draw()
        c.Write()
        
    of.Write()
    of.Close()
    f.Close()

if __name__ == '__main__':
    args = parse_args()
    plot_ip_res(args.input, args.output, args.sel)
