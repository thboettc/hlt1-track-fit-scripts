# hlt1-track-fit-scripts

Scripts for making plots to study the HLT1 track fit performance, including IP chi2, resolution, and pulls. `plot_ip_resolution.py` and `plot_ip_pulls.py` use output from the `IPResolutionCheckerNT`. `plot_track_pulls.py` plots the output from the `TrackResChecker`.