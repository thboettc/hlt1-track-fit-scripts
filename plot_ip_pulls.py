import os, sys
import ROOT
import argparse
from LHCbStyle import setLHCbStyle
setLHCbStyle()

def parse_args():
    parser = argparse.ArgumentParser(description='Script for plotting IP pulls.')
    parser.add_argument('--input', type=str, default=None, required=True,
                        help='Input ROOT file.')
    parser.add_argument('--output', type=str, default=None, required=True,
                        help='Output ROOT file.')
    parser.add_argument('--sel', type=str, default=None,
                        help='Track selection.')
    return parser.parse_args()

def plot_ip_pulls(fname, oname, sel=None):
    '''
    Plot IP pulls. 
    '''
    
    f = ROOT.TFile(fname, 'read')
    of = ROOT.TFile(oname, 'recreate')
    t = f.Get('TrackIPResolutionCheckerNT/tracks')
    of.cd()
    
    pull_vars = ['IPx', 'IPy', 'recIPx', 'recIPy']

    # By default select long tracks matched to prompt particles.
    if sel==None:
        sel = 'trueeta>2 && trueeta<5 && typeofprefix==0 && type==3 && truept>0.5'

    pt = ROOT.TPaveText(0.15, 0.75, 0.41, 0.9, 'BRNDC')
    pt.SetTextSize(0.055)
    pt.SetBorderSize(1)
    pt.SetFillColor(0)
    pt.SetFillStyle(0)
    pt.SetLineWidth(0)

    for pull_var in pull_vars:
        h = ROOT.TH1F('h_'+pull_var+'_pull', '', 50, -5, 5)
        draw_var = pull_var + '/' + pull_var + 'err'
        t.Draw(draw_var + '>>' + h.GetName(), sel, 'goff')
        h.GetXaxis().SetTitle(pull_var + ' pull')
        c = ROOT.TCanvas('c_'+pull_var+'_pull', 'c_'+pull_var+'_pull')
        h.Draw('E1 p1')
        func = ROOT.TF1('f_'+pull_var, 'gaus(0)', -5, 5)
        func.SetLineColor(ROOT.kRed)
        h.Fit('f_'+pull_var, 'R')
        pt.Clear()
        pt.AddText('#mu: ' + '{:.3f}#pm{:.3f}'.format(func.GetParameter(1),func.GetParError(1)))
        pt.AddText('#sigma: ' + '{:.3f}#pm{:.3f}'.format(func.GetParameter(2),func.GetParError(2)))
        print(40*'/\\')
        print(h.GetMean(), h.GetStdDev())
        pt.Draw()
        c.Write()

    of.Write()
    of.Close()
    f.Close()

if __name__ == '__main__':
    args = parse_args()
    plot_ip_pulls(args.input, args.output, args.sel)
